# encoding: utf-8
#Problem 4 †
#左右どちらから読んでも同じ値になる数を回文数という。 2桁の数の積で表される回文数のうち、最大のものは 9009 = 91 × 99 である。
#
#では、3桁の数の積で表される回文数のうち最大のものはいくらになるか。


######################################################
#yamaguchi さらにさらに寝る前に思いついちゃったのでかきかき
#更にさらに100ms程度高速化 これが今の私の限界
######################################################
start = Time.now
min = 100
max = 999
max_palindrome = 0
catch(:finish){
  #1番目の値を999,998...とループ
  max.downto(min){|num1|
    #2番目の値を999,998...とループ
    max.downto(min){|num2|
      #とりあえず乗算
      product = num1 * num2
      #乗積が現在の最大回文数より小さければ
      if product <= max_palindrome
        #num2がmax(999)と等しくても最大回文数より小さければ
        #これ以上処理を続けても意味ないので終了
        throw :finish if num2 == max
        #このnum1はスキップして次のnum1へ
        break
      end
      #乗積を配列に変換して、reverseした状態で等しければ回分数とする
      product_array = product.to_s.split ''
      if product_array == product_array.reverse
        max_palindrome = product
        #このnum1でこれ以上大きい回分数は出てこないので次のnum1へ
        break
      end
    }
  }
}
p max_palindrome #=> 906609
p ((Time.now - start) * 1000).round #=> 80ms前後

######################################################
#yamaguchi 寝る前に思いついちゃったのでかきかき
# 更に100msほど高速化
######################################################
start = Time.now
min = 100
max = 999
min_minus = min - 1
numbers = (min..max).to_a.reverse
products = numbers.product(numbers)
products_size = products.size
max_palindrome = 0
i = 0
while (i += 1) < products_size
  num1 = products[i][0]
  num2 = products[i][1]
  product = num1 * num2
  if product <= max_palindrome
    #num2がMaxと等しい場合、これ以上計算しても無意味なので終了
    break if num2 == max
    #num2の分だけiを足してnum1を次の値にする
    i += (num2 - min_minus)
    next
  end

  product_array = product.to_s.split ''
  if product_array == product_array.reverse
    max_palindrome = product
    i += (num2 - min_minus)
  end
end

p max_palindrome #=> 906609
p ((Time.now - start) * 1000).round #=> 180ms前後

######################################################
#yamaguchi 本間くんのを参考にリファクタリング後バージョンに追加して藤堂さんのを参考にリファクタリングバージョン
#速度は変わらないけどロジックがすごくシンプルでわかりやすくなりました
######################################################
start = Time.now
max_palindromes = 0
#[[999,998],[999,997]...]な配列を作って回す
numbers = (111..999).to_a.reverse
numbers.product(numbers).each{|ary|
  number = ary[0]*ary[1]
  next if number <= max_palindromes #掛けた数が現在の最大値より小さければスキップ

  #回文数かどうかを確認
  number_array = number.to_s.scan /./
  max_palindromes = number if number > max_palindromes && number_array == number_array.reverse
}
p max_palindromes #=> 906609
p ((Time.now - start) * 1000).round #=> 280ms前後


######################################################
#yamaguchi 本間くんのを参考にリファクタリング後バージョン
######################################################
start = Time.now
max_palindromes = 0
#[[999,998],[999,997]...]な配列を作って回す
numbers = (111..999).to_a.reverse
numbers.product(numbers).each{|ary|
  number = ary[0]*ary[1]
  next if number <= max_palindromes #掛けた数が現在の最大値より小さければスキップ

  #回文数かどうかを確認
  number_array = number.to_s.scan /./
  max_palindromes = number if number > max_palindromes && 0.upto((number_array.length / 2).to_i).all?{|key|number_array[key]==number_array[-1-key]}
}
p max_palindromes #=> 906609
p ((Time.now - start) * 1000).round #=> 280ms前後

######################################################
#yamaguchi 初期バージョン
######################################################
start = Time.now
def is_palindrome?(num)
  numbers = num.to_s.scan(/./)
  while numbers.length > 0
    if numbers.length == 1
      return true
    else
      left = numbers.shift
      right = numbers.pop
      if left != right
        return false
      elsif numbers.length == 0
        return true
      end
    end
  end
  return false
end
max = 999
min = 100
max_palindrome = 0
max.downto(min){|num1|
  max.downto(min){|num2|
    num = num1 * num2
    if is_palindrome?(num)
      max_palindrome = num if max_palindrome < num
      break
    end
  }
}
p max_palindrome #=> 906609
p ((Time.now - start) * 1000).round #=>2700ms前後

######################################################
#本間くんバージョン
######################################################
start = Time.now
n = (1..9).to_a
ary = n.product([0, *n], [0, *n]).map {|a, b, c| ("#{a}" + "#{b}" + "#{c}").to_i}
palindromic_ary = ary.product(ary).map {|a, b| a * b}.uniq.sort.reverse.select do |n|
  num_ary = n.to_s.split(//)
  0.step((num_ary.length / 2).to_i).to_a.all? {|i| num_ary[i] == num_ary[-1 - i]}
end
p palindromic_ary.max # => 906609
p ((Time.now - start) * 1000).round #=> 2000ms前後